<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DateandTimeController extends AbstractController
{ 
    /**
     * @Route("/datetime", name="vlad")
     */
    
    public function datetimenew(): Response{

        date_default_timezone_set("Europe/Bucharest");
        $currentdate = date("d.F.Y/D H:i:s");
        $diffDate = "";
        if (!empty($_GET["bday"]))
        { $date1 = date_create();
          $date2 = date_create($_GET["bday"]);
          /** 
            *   var_dump($date1);
            * var_dump($date2);
            * die(); */
          $interval = date_diff($date1, $date2);
          $diffDate=$interval ->format("%y");        }
        return $this->render("datetime.html.twig", [
            'currentdate' => $currentdate,
            'diffDate' => $diffDate
        ]
        );
        
    }
}