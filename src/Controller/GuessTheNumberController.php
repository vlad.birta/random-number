<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GuessTheNumberController extends AbstractController
{

    public $mySession;

    public function __construct(SessionInterface $session)
    { $this -> session = $session; }

    /**
     * @Route("/guess-the-number", name="vlad")
     */
    public function guessTheNumber(): Response
    {

        $theNumber = 0;
        $answer= "";
        if ($this -> session -> get('theNumber') != null) {
            $theNumber= $this->session->get('theNumber');
        } else {
            $this->session->set('theNumber', random_int(0, 100));
            $theNumber= $this->session->get('theNumber');
        }

        if (!empty($_GET["chosenNumber"])) {
            if ($_GET["chosenNumber"] == $theNumber) {
                $answer= "EXACT";
            } else if ($_GET["chosenNumber"] > $theNumber) {
                $answer = "PREA MARE";
            } else
                $answer= "PREA MIC";
        }

        return $this->render('guessTheNumber/guessTheNumber.html.twig', [
            'theNumber' => $theNumber,
            'answer' => $answer
        ]);
    }
}